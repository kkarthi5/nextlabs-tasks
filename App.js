import React, { useState } from 'react';

// Define the tree data
const tree = {
  name: 'root',
  children: [
    {
      name: 'folder1',
      children: [
        {
          name: 'file1.txt'
        },
        {
          name: 'file2.txt'
        }
      ]
    },
    {
      name: 'folder2',
      children: [
        {
          name: 'file3.txt'
        },
        {
          name: 'subfolder',
          children: [
            {
              name: 'file4.txt'
            }
          ]
        }
      ]
    }
  ]
};

// Define the TreeViewNode component
const TreeViewNode = ({ node, level, onAddNode, onEditNode, onDeleteNode }) => {
  const [isEditing, setIsEditing] = useState(false);
  const [nodeName, setNodeName] = useState(node.name);

  const handleAddNode = () => {
    onAddNode(node);
  };

  const handleEditNode = () => {
    onEditNode(node, nodeName);
    setIsEditing(false);
  };

  const handleDeleteNode = () => {
    onDeleteNode(node);
  };

  const handleNodeNameChange = (event) => {
    setNodeName(event.target.value);
  };

  const handleKeyDown = (event) => {
    if (event.key === 'Enter') {
      handleEditNode();
    }
  };

 

  return (
    <div style={{backgroundColor:"aquamarine"}}>
      <div style={{paddingTop:"10px",marginTop:"0px"}}>
        <span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
        <span>
          {isEditing ? (
            <input type="text" value={nodeName} onChange={handleNodeNameChange} onKeyDown={handleKeyDown} />
          ) : (
            <span onClick={() => setIsEditing(true)}>{node.name}</span>
          )}
        </span>
        <span >
          <button onClick={handleAddNode}>Add</button>
          <button onClick={handleEditNode}>Edit</button>
          <button onClick={handleDeleteNode}>Delete</button>
        </span>
      </div>
      {node.children && node.children.map((childNode, index) => (
        <TreeViewNode
          key={index}
          node={childNode}
          level={level + 1}
          onAddNode={onAddNode}
          onEditNode={onEditNode}
          onDeleteNode={onDeleteNode}
        />
      ))}
    </div>
  );
};

// Define the App component
const App = () => {
  const [treeData, setTreeData] = useState(tree);

  const handleAddNode = (parentNode) => {
    const nodeName = window.prompt('Enter a name for the new node');
    if (!nodeName) {
      return;
    }

    const newNode = { name: nodeName };
    parentNode.children.push(newNode);
    setTreeData({ ...treeData });
  };

  const handleEditNode = (node, newName) => {
    node.name = newName;
    setTreeData({ ...treeData });
  };

  const handleDeleteNode = (node) => {
    const parentNode = findParentNode(treeData, node);
    parentNode.children = parentNode.children.filter(childNode => childNode !== node);
    setTreeData({ ...treeData });
  };

  const findParentNode = (treeData, node) => {
    if (treeData.children && treeData.children.includes(node)) {
      return treeData;
    }

    for (let i = 0; i < treeData.children.length; i++) {
      const parentNode = findParentNode(treeData.children[i],  node);
      if (parentNode) {
        return parentNode;
      }
    }
    
    return null;
  };

  return (
   <div >
     <center> 
      <h1>Creating an folder structure</h1>
      <div style={{border:"2px solid black",width:"500px",marginTop:"0%"}}>
  <TreeViewNode
       node={treeData}
       level={0}
       onAddNode={handleAddNode}
       onEditNode={handleEditNode}
       onDeleteNode={handleDeleteNode}
     /></div></center>
  </div>
  );
  };
  
  export default App;
